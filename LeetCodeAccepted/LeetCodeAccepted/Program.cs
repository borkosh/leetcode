﻿using System;

namespace LeetCodeAccepted
{
    class Program
    {
        static void Main(string[] args)
        {

            string str = "-000000000000001";
            int res = MyAtoi(str);
            Console.WriteLine(res);
            Console.ReadKey();
        }

        public static Int32 MyAtoi(string str)
        {
            string alpha = string.Empty;
            string number = string.Empty;
            int sign=1;
            Int64 int64num = 0;
            Int32 int32num = 0;
            str = str.Trim();

            if ((str.Length == 0)) return 0;


            if (str[0] == '-') {
                sign = -1;
                if (str.Length == 1) return 0;
                if (!char.IsDigit(str[1])) return 0;
            }
            if (str[0] == '+') {
                sign = 1;
                if (str.Length==1) return 0;
                if (!char.IsDigit(str[1])) return 0;
            }
            str = str.TrimStart('-');
            str = str.TrimStart('+');
            str = str.TrimStart('0');

            for (int i= 0; i < str.Length;i++) {
                if (char.IsDigit(str[i]))
                {
                    number += str[i];
                }else { break; }

                if (number.Length > 10)
                {
                    if (sign<0)
                    {
                        return Int32.MinValue;
                    }
                    return Int32.MaxValue;
                }
            }

            if (number.Length > 0)
            {
                int64num= Int64.Parse(number) * sign;
                if (int64num >= Int32.MaxValue) {
                    return Int32.MaxValue;
                }
                if (int64num <= Int32.MinValue)
                {
                    return Int32.MinValue;
                }

                return Int32.Parse(number) * sign;
            }
            return 0;
        }
        public static bool IsPalindrome(int x)
        {
            string str = x.ToString();
            char[] arrStr = str.ToCharArray();
            Array.Reverse(arrStr);
            if (str==new string(arrStr))
            {
                return true;
            }
            return false;
        }

        public static int[] TwoSum(int[] nums, int target)
        {
            int[] result=new int[2];
            for (int i = 0; i < nums.Length-1; i++)
                for (int j = i+1; j < nums.Length; j++) {
                    if(nums[i]== target-nums[j])
                    {
                        result[0]= i;
                        result[1] = j;
                        return result;
                    }
                }
            return null;
        }

        public static int Reverse(int x)
        {
            int newInt = 0;
            while (x != 0)
            {
                int remainder = x % 10;
                x = x / 10;
                if (newInt > Int32.MaxValue / 10 || (newInt == Int32.MaxValue / 10 && remainder > 7))
                {
                    return 0;
                }
                if (newInt < Int32.MinValue / 10 || (newInt == Int32.MinValue / 10 && remainder < -8))
                {
                    return 0;
                }
                newInt *= 10;
                newInt += remainder;

            }
            return newInt;
        }
    }
}
